// @ts-check
// 基础静态类型
const shuzi: number = 21;            //定义数字类型
const myName: string = 'LiuZhenXiu'  //定义字符串类型

// 自定义静态类型
interface Yellow {
  uname: string;
  age: number;
}

// 自定义静态类型
const myYellow: Yellow = {
  uname: "浑南狗少",
  age: 22,
};

// 对象类型
const xiaoJieJie: {
  name: string,
  age: number,
} = {
  name: "珍秀",
  age: 18,
};
console.log(xiaoJieJie.name);

const xiaoJieJies: String[] = ["秀姐", "秀秀", "珍秀"];

// 定义类
class Person { }
const dajiao: Person = new Person();
const jianXiaoJieJie: () => string = () => {
  return "秀秀啊!!";
};