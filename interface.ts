// 要开始招聘小姐姐了，这时候你需要一些小姐姐的简历投递和自动筛选功能，
// 就是不符合简历要求的会直接被筛选掉，符合的才可以进入下一轮的面试。
// 那最好的解决方法就是写一个接口。TypeScript 中的接口，就是用来规范类型的

/**
 * 年龄小于 25 岁，胸围大于 90 公分的
 * @param name 姓名
 * @param age 年龄
 * @param bust 胸围
 */
const screenResume = (name: string, age: number, bust: number) => {
  age < 24 && bust >= 90 && console.log(name + "进入面试");
  age > 24 || (bust < 90 && console.log(name + "你被淘汰"));
};

screenResume("秀秀", 21, 110);


// 老板又增加了需求，说我必须能看到这些女孩的简历
const getResume = (name: string, age: number, bust: number) => {
  console.log(name + "年龄是：" + age);
  console.log(name + "胸围是：" + bust);
};
getResume("秀秀", 21, 110);


// 两个方法用的类型注解一样，需要作个统一的约束。大上节课我们学了一个类型别名的知识可以解决代码重复的问题，这节课我们就学习一个更常用的语法接口（Interface）

// 把这两个重复的类型注解，定义成统一的接口
interface Girl {
  name: string;
  age: number;
  bust: number;
  waistline?: number;//接口非必选值得定义  在:号前加一个?
  [propname: string]: any;//允许加入任意值  属性的名字是字符串类型，属性的值可以是任何类型
  say(): string;  //接口里的方法  这样写 对应的对象里也必须有这个方法 下面不写报错
}
const screenResumeTwo = (girl: Girl) => {
  girl.age < 24 && girl.bust >= 90 && console.log(girl.name + "进入面试");
  girl.age > 24 || (girl.bust < 90 && console.log(girl.name + "你被淘汰"));
  girl.waistline && console.log(girl.name + "腰围是：" + girl.waistline);
};

const getResumeTwo = (girl: Girl) => {
  console.log(girl.name + "年龄是：" + girl.age);
  console.log(girl.name + "胸围是：" + girl.bust);
};
const girl = {
  name: "秀秀",
  age: 21,
  bust: 110,
  say() {
    return "你笑起来真好看";
  },
  teach() {
    return "我是一个老师";
  },
};

screenResumeTwo(girl);
getResumeTwo(girl);

// 类型别名可以直接给类型，比如string，而接口必须代表对象


// 接口和类的约束
//  JavaScript 从ES6里是有类这个概念的，类可以和接口很好的结合

// class XiaoJieJie implements Girl {} //单独这么写是报错的

class XiaoJieJie implements Girl {
  name = "秀秀";
  age = 21;
  bust = 110;
  say() {
    return "你笑起来真好看";
  }
}

// 接口间的继承
// 接口也可以用于继承的，比如你新写一个Teacher接口，继承于Person接口
interface Teacher extends Girl {
  teach(): string;
}
// 只看taecher级别的
const getResumeThree = (girl: Teacher) => {
  console.log(girl.name + "年龄是：" + girl.age);
  console.log(girl.name + "胸围是：" + girl.bust);
  girl.waistline && console.log(girl.name + "腰围是：" + girl.waistline);
  girl.sex && console.log(girl.name + "性别是：" + girl.sex);
};
getResumeThree(girl);