// TypeScript 中类的概念和使用
// TypeScript 中类的概念和 ES6 中原生类的概念大部分相同，但是也额外增加了一些新的特性。

// 类的基本使用

class Lady {
  content = "浑南狗少,你瞅啥呢";
  sayHello() {
    return this.content;
  }
}

const goddess = new Lady();
console.log(goddess.sayHello());

// 类的继承
// TypeScrip 的继承和ES6中的继承是一样的。关键字也是extends,比如我们这里新建一个XiaoJieJie的类，然后继承自Lady类，在XiaoJieJie类里写一个新的方法，叫做sayLove,具体代码如下。
class LadyTwo {
  content = "浑南狗少,你瞅啥呢";
  sayHello() {
    return this.content;
  }
}
class XiaoJieJie extends Lady {
  sayLove() {
    return "Hi";
  }
}

const goddessTwo = new XiaoJieJie();
console.log(goddessTwo.sayHello());
console.log(goddessTwo.sayLove());

// 类的重写
// 讲了继承，那就必须继续讲讲重写，重写就是子类可以重新编写父类里边的代码。现在我们在XiaoJieJie这个类里重写父类的sayHello()方法
class XiaoJieJieTwo extends Lady {
  sayLove() {
    return "I love you!";
  }
  sayHello() {
    return "Hi , honey!";
  }
}
const goddessThree = new XiaoJieJieTwo();
console.log(goddessThree.sayHello());
console.log(goddessThree.sayLove());

// super关键字的使用，比如我们还是想使用Lady类中说的话，但是在后面，加上你好两个字就可以了。这时候就可以使用super关键字，它代表父类中的方法。
class XiaoJieJieThere extends Lady {
  sayLove() {
    return "I love you!";
  }
  sayHello() {
    return super.sayHello() + "。你好！";
  }
}
const goddessFour = new XiaoJieJieThere();
console.log(goddessFour.sayHello());
console.log(goddessFour.sayLove());


// 简单学习了TypeScript中类的使用，类中的访问类型。其实类的访问类型就是基于三个关键词private、protected和public,也是三种访问类型
class Person {
  name: string;
}

const person = new Person();
person.name = "秀秀真好看";

console.log(person.name);

// public从英文字面的解释就是公共的或者说是公众的，在程序里的意思就是允许在类的内部和外部被调用
class PersonTwo {
  public name:string;
  public sayHello(){
      console.log(this.name + 'say Hello')
  }
}
//-------以下属于类的外部--------
const personTwo = new PersonTwo()
personTwo.name = '秀秀真好看'
personTwo.sayHello()
console.log(personTwo.name)

// private 访问属性的意思是，只允许再类的内部被调用，外部不允许调用
class PersonThree {
  private name:string;
  public sayHello(){
      console.log(this.name + 'say Hello')  //此处不报错
  }
}
//-------以下属于类的外部--------
const personThree = new PersonThree()
//personThree.name = 'jspang.com'    //此处报错
personThree.sayHello()
//console.log(personThree.name)  //此处报错

// protected 允许在类内及继承的子类中使用
class PersonFour {
  protected name:string;
  public sayHello(){
      console.log(this.name + 'say Hello')  //此处不报错
  }
}

class Teacher extends PersonFour{
  public sayBye(){
      this.name;
  }
}