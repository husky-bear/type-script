[![Fork me on Gitee](https://gitee.com/husky-bear/type-script/widgets/widget_6.svg?color=undefined)](https://gitee.com/husky-bear/type-script)
# TypeScript

#### 介绍
Study TypeScript

#### 软件架构
ts-node [文件全名+后缀]


#### 安装教程

typescript@4.0.3
ts-node@9.0.0
@vue/cli@4.5.6

#### 参与贡献

[![HuskyBear/TypeScript](https://gitee.com/husky-bear/type-script/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/husky-bear/type-script)


