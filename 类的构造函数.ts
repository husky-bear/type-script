class Person{
  public name :string ;
  // 构造器
  constructor(name:string){
      this.name=name
  }

}

const person= new Person('秀秀')
console.log(person.name)

// 简写 相当于你定义了一个name,然后在构造函数里进行了赋值
class Person2{
  public name :string ;
  constructor(name:string){
      this.name=name
  }

}

const person2= new Person2('秀秀2')
console.log(person2.name)