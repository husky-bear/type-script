// @ts-check


//annotation 类型注解

//  count 变量就是一个数字类型，这就叫做类型注解
let count: number;
count = 123;

//    并没有显示的告诉你变量countInference是一个数字类型，但是如果你把鼠标放到变量上时，
// 你会发现 TypeScript 自动把变量注释为了number（数字）类型，也就是说它是有某种推断能
// 力的，通过你的代码 TS 会自动的去尝试分析变量的类型
let countInference = 123;


// 如果 TS 能够自动分析变量类型， 我们就什么也不需要做了
// 如果 TS 无法分析变量类型的话， 我们就需要使用类型注解

// 不用写类型注解的例子
const one = 1;
const two = 2;
const three = one + two;


// 用写类型注解的例子
// function getTotal(one, two) {
//   return one + two;
// }
function getTotal(one: number, two: number):number{
  return one + two;
}
// total这个变量不需要加类型注解，因为当one和two两个变量加上注解后，TypeScript 就可以自动通过类型推断，分析出变量的类型
const total = getTotal(1, 2);

const XiuXiu = {
  name: "秀秀",
  age: 21,
};


