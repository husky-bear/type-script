// @ts-check

const numberArr: number[] = [1, 2, 3];
// 数组各项是字符串
const stringArr: string[] = ["a", "b", "c"];
// 定义任意类型的数组，比如是undefined
const undefinedArr: undefined[] = [undefined, undefined];

// 如果数组中有多种类型，比如既有数字类型，又有字符串的时候。只要加个()，然后在里边加上|就可以了
const arr: (number | string)[] = [1, "string", 2];

// 对于这类带有对象的数组定义就稍微麻烦点了。 比如现在我们要定义一个有很多美女的数组，每一个美女都是一个对象。这是的定义就编程了这样
const meinv: { name: string, age: Number }[] = [
  { name: "秀秀", age: 21 },
  { name: "珍秀", age: 21 },
];

// 这种形式看起来比较麻烦，而且如果有同样类型的数组，写代码也比较麻烦，TypeScript 为我们准备了一个概念，叫做类型别名(type alias)
// 比如刚才的代码，就可以定义一个类型别名，定义别名的时候要以type关键字开始。现在定义一个Lady的别名
type Lady = { name: string, age: Number };
const Meinv: Lady[] = [
  { name: "秀秀", age: 21 },
  { name: "珍秀", age: 21 },
];

// 定义一个Madam的类,然后用这个类来限制数组的类型
class Madam {
  name: string;
  age: number;
}

const madam: Madam[] = [
  { name: "秀秀", age: 21 },
  { name: "珍秀", age: 21 },
];
