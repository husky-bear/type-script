// @ts-check

// function getTotal(one: number, two: number) {
//   return one + two;
// }

// const total = getTotal(1, 2);

//    没有定义getTotal的返回值类型，虽然TypeScript可以自己推断出返回值是number类型。 
// 但是如果这时候我们的代码写错了，比如写程了下面这个样子。

// function getTotal(one: number, two: number) {
// 这时候total的值就不是number类型了，但是不会报错。有的小伙伴这时候可能会说，可以直接给total一个类型注解
//   return one + two + "";
// }

//    可以让编辑器报错，但是这还不是很高明的算法，因为你没有找到错误的根本，
// 这时错误的根本是getTotal()函数的错误，
// 所以合适的做法是给函数的返回值加上类型注解
// const total: number = getTotal(1, 2);



function getTotal(one: number, two: number): number {
  return one + two;
}

const total: number = getTotal(1, 2);


// 有时候函数是没有返回值的，比如现在定义一个sayHello的函数，这个函数只是简单的terminal打印，并没有返回值
// function sayHello() {
//   console.log("hello world");
// }
// 一个类型注解void，代表没有任何返回值,这样定义后，你再加入任何返回值，程序都会报错
function sayHello(): void {
  console.log("hello world");
}

// never 返回值类型
//    如果一个函数是永远也执行不完的，就可以定义返回值为never，
// 那什么样的函数是永远也执行不完的那?
// 我们先来写一个这样的函数(比如执行执行的时候，抛出了异常，这时候就无法执行完了)。
function errorFuntion(): never {
  throw new Error();
  console.log("Hello World");
}
// 还有一种是一直循环，也是我们常说的死循环，这样也运行不完，比如下面的代码：
function forNever(): never {
  while (true) {}
  console.log("Hello JSPang");
}

// 函数参数为对象(解构)时
function add({ one, two }: { one: number, two: number }): number {
  return one + two;
}

const three = add({ one: 1, two: 2 });

function getNumber({ one }: { one: number }): number {
  return one;
}

const one = getNumber({ one: 1 });
